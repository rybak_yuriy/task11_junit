package com.rybak.view;

import com.rybak.model.LongestPlateau;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class PlateauView {

    Scanner scanner = new Scanner(System.in);
    int[] array;
    private LongestPlateau longestPlateau;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;


    public PlateauView() {
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        longestPlateau = new LongestPlateau();
        array = new int[0];

        menu.put("1", "1 - Enter array");
        menu.put("2", "2 - Get length of plateau");
        menu.put("3", "3 - Get location of plateau");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::getArray);
        menuMethods.put("2", this::outputLongestPlateau);
        menuMethods.put("3", this::getLocation);
    }


    public void getArray() {
        System.out.println("Enter number of integers:");
        array = new int[scanner.nextInt()];
        System.out.println("Enter numbers:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
    }

    public void outputLongestPlateau() {
        System.out.println(longestPlateau.getLongestPlateau(array));

    }

    private void getLocation() {
        System.out.println(longestPlateau.getLocation(array));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println(" Q - quit");
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }
}

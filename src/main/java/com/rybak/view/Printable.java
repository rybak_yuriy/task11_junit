package com.rybak.view;

@FunctionalInterface
public interface Printable {

    void print();
}

package com.rybak;

import com.rybak.view.PlateauView;

public class App {

    public static void main(String[] args) {
        new PlateauView().start();
    }
}
